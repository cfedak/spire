export class SpireActor extends Actor {
  /** @override */
  prepareData() {
    super.prepareData();
  }

  /** @override */
  prepareDerivedData() {
    const actorData = this;
    const systemData = actorData.system;

    let totalStress = 0;
    for (let resistance in systemData.resistances) {
      if (resistance == "armor") {
        continue;
      }
      let actualStress =
        systemData.resistances[resistance].stress -
        systemData.resistances[resistance].value;
      actualStress = Math.max(actualStress, 0);
      totalStress += actualStress;
    }
    systemData.totalStress = totalStress;
  }
}
