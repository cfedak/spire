export function getItemDescription(item) {
  let itemDescription;
  if (item.type === "equipment") {
    itemDescription = $(
      `<div class="spire-table-item-summary spire-tag-list"></div>`
    );
    // TODO: a loop would probably be more appropriate
    if (item.system.tag1) {
      itemDescription.append(
        `<span class="spire-tag">${item.system.tag1}</span> `
      );
    }
    if (item.system.tag2) {
      itemDescription.append(
        `<span class="spire-tag">${item.system.tag2}</span> `
      );
    }
    if (item.system.tag3) {
      itemDescription.append(
        `<span class="spire-tag">${item.system.tag3}</span> `
      );
    }
    if (item.system.tag4) {
      itemDescription.append(
        `<span class="spire-tag">${item.system.tag4}</span> `
      );
    }
    if (item.system.tag5) {
      itemDescription.append(
        `<span class="spire-tag">${item.system.tag5}</span>`
      );
    }
  } else {
    itemDescription = $(
      `<div class="spire-table-item-summary">${item.system.description}</div>`
    );
  }
  return itemDescription;
}

// Find the checkbox/radio checked with the given name
export function findChecked(html, name) {
  let elements = html.find(`[name="${name}"]`);
  for (let element of elements) {
    if (element.checked) {
      return element.value;
    }
  }
}
